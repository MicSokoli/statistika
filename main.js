// Will have properties where the property name is the value of the input
// and the property's value is the frequency for that input
var inputsWithFrequenciesObject;
// Will hold arrays where the first element is a string representing the value of the input
// and the second string holds the frequency for that input
var inputsWithFrequenciesArray;
// Will hold arrays where the first element is a string representing the bounds of the group
// and the second string holds the sum of frequencies of the elements in that group
var groupedInputsWithFrequenciesArray;
// Will hold every data point as a number, with repetitions, sorted.
var sortedInputsArray;
// Will hold the total number of values given by the user
var numberOfInputs;
// Will hold an array of arrays that contain the name of a statistical property as the first element
// and the value of that property as the second element.
var statisticalAttributes;

$(document).ready(function () {
    $("#analyze-button").click(function () {
        analyze();
    }); 
    $("#grouping-check").change(function () {
        $("#group-size-container").toggle();
        $("#highcharts-container-grouped").toggle();        
    });
});

function analyze() {
    // All arrays and object that hold the raw data are reset before doing calculations
    inputsWithFrequenciesObject = new Object();
    inputsWithFrequenciesArray = new Array();
    groupedInputsWithFrequenciesArray = new Array();
    sortedInputsArray = new Array();

    fetchRawData();
	drawChart();

    if ($("#grouping-check").is(':checked')) {
        groupData();
        drawGroupedChart();
    }    
    findStatisticalAttributes();
    displayStatisticalAttributes();
}


function fetchRawData() {
    var rawDataString = $("#raw-data-text-area").val(); 
    // replace all newlines with spaces 
    rawDataString = rawDataString.replace(/\n/gm,' '); 
    // replace all tabs with spaces 
    rawDataString = rawDataString.replace(/\t/g,' ');      
    // replace all double spaces with single spaces
    rawDataString = rawDataString.replace(/\s+/g, ' ');;
  

    rawDataString = rawDataString.trim();
    inputsAsStrings = rawDataString.split(' ');
    $(inputsAsStrings).each(function (key, currentValue) {        
        if(currentValue != "") {
            if (inputsWithFrequenciesObject[currentValue] == undefined) {
                inputsWithFrequenciesObject[currentValue] = 1;
            }
            else {
                inputsWithFrequenciesObject[currentValue]++;
            } 
        }       
    }); 

    for (index in inputsWithFrequenciesObject) {
        inputsWithFrequenciesArray.push([index, inputsWithFrequenciesObject[index]]);
    }
    for (index in inputsAsStrings) {
        sortedInputsArray.push(+inputsAsStrings[index]);
    }
    numberOfInputs = sortedInputsArray.length;
    sortedInputsArray.sort();
}

function groupData() {
    var groupSize = +$("#group-size-input").val();
    var dataMinVal = Math.min.apply(Math, sortedInputsArray);
    var dataMaxVal = Math.max.apply(Math, sortedInputsArray);

    for (var i = dataMinVal; i <= dataMaxVal; i += groupSize) {
        var currentGroupFrequency = 0;
        var currentGroupMin = i;
        var currentGroupMax = i + groupSize - 1;

        for (var j = i; j < i + groupSize; j++) {
            var currentValueFrequency =  inputsWithFrequenciesObject[j];
            if (currentValueFrequency) {
                currentGroupFrequency += currentValueFrequency;
            }
        }
        groupedInputsWithFrequenciesArray.push([currentGroupMin + " - " + currentGroupMax,  currentGroupFrequency]);        
    }
}

function drawChart() {
	$(function () {
        $('#highcharts-container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Analiza statistikore'
            },
            subtitle: {
                text: 'Shfaqja e vlerave të ndryshores X sipas dendurive'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'f(x)'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Denduria {point.y:.1f}</b>',
            },
            series: [{
                name: 'Denduritë',
                data: inputsWithFrequenciesArray,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center',
                    x: 0,
                    y: 30,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
        });
    });
}

function drawGroupedChart() {
    $(function () {
        $('#highcharts-container-grouped').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Analiza statistikore për të dhëna të grupuara'
            },
            subtitle: {
                text: 'Shfaqja e vlerave të ndryshores X sipas dendurive të grupuara'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'f(x)'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Denduria {point.y:.1f}</b>',
            },
            series: [{
                name: 'Denduritë',
                data: groupedInputsWithFrequenciesArray,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center',
                    x: 0,
                    y: 30,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
        });
    });
}

function findStatisticalAttributes() {
    statisticalAttributes = new Array();

    statisticalAttributes.push(["All data", sortedInputsArray]);
    statisticalAttributes.push(["Number of data points", numberOfInputs]);

    var dataMinVal = Math.min.apply(Math, sortedInputsArray);
    statisticalAttributes.push(["Minimum", dataMinVal]);
    var dataMaxVal = Math.max.apply(Math, sortedInputsArray);    
    statisticalAttributes.push(["Maximum", dataMaxVal]);

    var sumOfAllInputs = getSumOfAllInputs();
    statisticalAttributes.push(["Sum of all inputs", sumOfAllInputs]);
    var arithmeticMean = getArithmeticMean(sumOfAllInputs);
    statisticalAttributes.push(["Arithmetic mean", arithmeticMean]);
    var standardDeviation = getStandardDeviation(arithmeticMean);
    statisticalAttributes.push(["Standard Deviation", standardDeviation]);
    var variationCoefficient = standardDeviation / arithmeticMean;
    statisticalAttributes.push(["Variation Coefficient", variationCoefficient]);    
}

function getSumOfAllInputs () {
    var sumOfAllInputs = 0;
    for (var i = 0; i < numberOfInputs; i += 1) {
        sumOfAllInputs += sortedInputsArray[i];
    }
    return sumOfAllInputs;
}

function getArithmeticMean (sumOfAllInputs) {
    arithmeticMean = sumOfAllInputs / numberOfInputs;
    return arithmeticMean;
}

function getStandardDeviation (arithmeticMean) {
    var standardDeviation = 0;
    for (var i = 0; i < numberOfInputs; i += 1) {
        standardDeviation += Math.pow(sortedInputsArray[i], 2);
    }
    standardDeviation = standardDeviation / numberOfInputs;
    standardDeviation = standardDeviation - Math.pow(arithmeticMean, 2);
    standardDeviation = Math.sqrt(standardDeviation);
    return standardDeviation;
}

function displayStatisticalAttributes() {
    $("#statistical-attributes").text("");
    $(statisticalAttributes).each(function (index, attributeNameAndValue) {
        $("#statistical-attributes").append(
            "<p>" + 
            attributeNameAndValue[0] + ": " + attributeNameAndValue[1] + 
            "</p>"
            );
        console.log(attributeNameAndValue);
    });
}

