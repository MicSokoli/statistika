# README #

This repository stores the application 'statistika', used for doing some simple analysis and visualization of data given by the users.

### What is this application for? ###

* This application is created to help Computer Science students from the University of Prishtina with the exercises from the subject "Statistika". It does some of the calculations that students have to do for the exercises so that the students can verify their results.
* The application is not meant as a general purpose tool for statistical analysis. The goal is to provide a simple tool for the students to verify their results.
* Version 1.0

### How do I get set up? ###

* Download the repository to you computer and unarchive it
* Open the file index.html with a browser that has javascript enabled

### How do I use it? ###
* Currently the application only takes a sequence of numbers, calculates the frequencies for every value given and returns a graph that plots those frequencies, and another graph that plots the frequencies for groups of numbers.
* You can enter numbers via a text field. The numbers should be separated by spaces, newlines or tabs. Then you click on the "Analizo" button and the application plots the graphs.
* You can set the size of groups of disable data grouping, in which case only one graph will be plotted.

### How was it created? ###
* I have used HTML, CSS, JavaScript, jQuery, Bootstrap and HighCharts.

### Contribution guidelines ###

* Students that have taken this course can clone the repository and send me pull requests.
*